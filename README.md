# Inversify Express Application

This is an example project on how to use [Inversify](http://inversify.io) together with [Express](https://expressjs.com) using [inversify-express-utils](https://github.com/inversify/inversify-express-utils). This enables you to use dependency injection when building your express applications, which has the following advantages:
- Decoupling the creation and usage of objects.
- Easier to write unit tests as mocking classes becomes easier.
- Improved logging as the logger has access to the request context and can therefore output more detailed information (such as request, response and authenticated user) for each log output.

## Prerequisites

This project is using [Docker](https://www.docker.com) and [Docker-Compose](https://docs.docker.com/compose) to make it easier to get all services (Mongodb, Elasticsearch and Kibana) up and running. But there is nothing stopping you from running this without Docker, just that you will have to start and configure each service manually.

## Get up and running

To start all services all you have to do is run the following command:

```bash
# This will start Mongodb, Elasticsearch, Kibana and the Inversify example application.
./run.sh
```

This will start all the required services and they will then be available at the following urls:
- Inversify example application: http://localhost:3000/health
- Kibana: http://localhost:5601
- Elasticsearch: http://localhost:9200
- Fluentd: localhost:24224

## Development

If you wish to run the application in development mode, you can easily do that by executing the command below. This will only start the services required for development which is Mongodb and the Inversify example application. When the example application is started it will automatically start to listen for any file changes and automatically transpile the files and restart the application when a file change is detected.

```bash
# This will start Mongodb and the Inversify example application in development mode.
./run-dev.sh
```

## Example Requests

To test that everything is working as expected there are some example requests available in the "examples" folder that you can run. Start with executing the `./examples/health.sh` script, to see that the Inversify example application is up and running.

```bash
# This command should return a json object with the status UP if the application is fully up and running.
./examples/health.sh
```

When the above script returns `UP` you can continue and run the requests listed below:

```bash
# Add a new user
./examples/add-user.sh

# Login as the newly created user
./examples/login-user.sh

# Access secret endpoint (only accessible while logged in)
./examples/get-secret.sh

# Get all users in the database (the result is paginated, by default max 10 users are returned)
./examples/get-users.sh

# Try to add an invalid user (this should fail as the user isn't valid, this is to test the error handling)
./examples/add-invalid-user.sh

# Logout
./examples/logout-user.sh

# Get service metrics
./examples/metrics.sh
```

## Logging

When running in "production" mode all logs will be sent to [Fluentd](https://www.fluentd.org) where the logs will be transformed into a common log format and then sent to [Elasticsearch](https://www.elastic.co/elasticsearch) where they can later be retrieved and displayed by [Kibana](https://www.elastic.co/guide/en/kibana/current/introduction.html).

All log output (from the Inversify example application) contains the following information and can viewed and filtered in Kibana:
- User id (The id of the user who was authenticated)
- Request id (Unique id of the request. All log outputs for a single request will have the same request id)
- Request url
- Request method
- Request body
- Response status code
- Response response time
- Response content length
- Service name (The name of the Docker compose service)
- Git repository name (The name of this repository)
- Git repository namespace (The name of the Gitlab repository namespace)
- Git commit id (The id of the HEAD commit that this application was built/started on)
- Error stack (The stack trace of the error that is catched and logged by the express error handler middleware)

## Sentry

When a log output is made on error level it will be automatically (if configured) sent to Sentry togheter with the log message and the log context. This makes it possible to get notifications when errors occurs in the application.

## TODO
- Add Prometheus and Grafana for monitoring.
- Add another application to demonstrate tracing using Jaeger.
- Add integration tests.
- Add more unit tests.
- Parse mongodb log output into the same format as Kibana and the Inversify example application.
