import { Config } from '../../../src/interfaces';
import container from '../../../src/inversify-config';
import { TYPES } from '../../../src/types';

const setConfigMock = (config): void => {
  const configMock: Config = {
    get: name => {
      return config[name];
    },
    prettyPrint: (): string => {
      return '';
    }
  };

  container.unbind(TYPES.Config);
  container.bind<Config>(TYPES.Config).toConstantValue(configMock);
};

export default setConfigMock;
