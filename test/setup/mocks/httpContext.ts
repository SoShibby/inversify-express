import { interfaces, TYPE as InversifyTypes } from 'inversify-express-utils';
import container from '../../../src/inversify-config';

const setHttpContextMock = (
  request?,
  response?,
  user?,
  inversifyContainer?
): void => {
  const httpContext: any = {
    request,
    response,
    user,
    container: inversifyContainer
  };

  container
    .bind<interfaces.HttpContext>(InversifyTypes.HttpContext)
    .toConstantValue(httpContext);

  return httpContext;
};

export default setHttpContextMock;
