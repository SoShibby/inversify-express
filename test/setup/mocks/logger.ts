import { Logger } from '../../../src/interfaces';
import container from '../../../src/inversify-config';
import { TYPES } from '../../../src/types';

const setLoggerMock = (): Logger => {
  const mock = {
    info: jest.fn(),
    debug: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
    log: jest.fn()
  };

  container.unbind(TYPES.Logger);
  container.bind<Logger>(TYPES.Logger).toConstantValue(mock);

  return mock;
};

export default setLoggerMock;
