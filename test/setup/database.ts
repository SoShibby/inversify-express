import * as mongoose from 'mongoose';

const database = {
  connect: async () => {
    const url = process.env.MONGO_URL;

    if (!url.includes('/jest?')) {
      throw new Error(
        `Mongo url is possibly incorrect, expected the url to contain the string "/jest?" to indicate that this is a test database, but got ${url}.`
      );
    }

    await mongoose.connect(
      global.process.env.MONGO_URL,
      { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
      err => {
        if (err) {
          console.error(err);
          process.exit(1);
        }
      }
    );
  },
  disconnect: async () => {
    await mongoose.disconnect();
  },
  clearAll: async () => {
    const collections = Object.keys(mongoose.connection.collections);

    // eslint-disable-next-line no-restricted-syntax
    for (const collectionName of collections) {
      const collection = mongoose.connection.collections[collectionName];
      // eslint-disable-next-line no-await-in-loop
      await collection.deleteMany();
    }
  }
};

export default database;
