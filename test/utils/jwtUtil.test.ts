import { User } from '../../src/interfaces';
import { JWTUtil } from '../../src/utils';

describe('JWTUtil', () => {
  const validUser: User = {
    _id: '5099803df3f4948bd2f98391',
    username: 'test-user',
    firstName: 'john',
    lastName: 'doe',
    password:
      'cd9dc05e5ac4efc6c9dc84d9bae79e01d8c48299908f7b477f7803941a0ef68259c7a9af7ffe4427e6bb8494915bcbe3205e9b66a286d8f81887c3726a5078bb'
  };
  const validSecretKey = 'this-is-a-secret-key';

  const addHours = (date, hours): number => {
    return new Date(date.getTime() + hours * 60 * 60 * 1000).getTime();
  };

  const sleep = async (sleepInMs): Promise<void> =>
    new Promise(resolve => setTimeout(resolve, sleepInMs));

  test('should create a jwt token string', async () => {
    const jwt = JWTUtil.createJWT(validUser, validSecretKey);
    expect(typeof jwt).toBe('string');
    expect(jwt.substring(0, 2)).toBe('ey');
  });

  test('should create a valid user jwt token', async () => {
    const jwt = JWTUtil.createJWT(validUser, validSecretKey);

    const parsedJWT = JWTUtil.parseJWT(jwt);
    // The user JWT should only contain the _id of the user
    // We don't want to discolse personal information and/or password,
    // as the JWT content can be read client-side.
    expect(parsedJWT.user).toStrictEqual({
      _id: validUser._id
    });
  });

  test('jwt should be valid for 24 hours', async () => {
    const dateBeforeJWTCreation = new Date();
    await sleep(10);
    const jwt = JWTUtil.createJWT(validUser, validSecretKey);
    await sleep(10);
    const dateAfterJWTCreation = new Date();

    const parsedJWT = JWTUtil.parseJWT(jwt);

    expect(parsedJWT.exp).toBeGreaterThan(addHours(dateBeforeJWTCreation, 24));
    expect(parsedJWT.exp).toBeLessThan(addHours(dateAfterJWTCreation, 24));
  });
});
