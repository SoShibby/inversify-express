import 'reflect-metadata';
import container from '../../../src/inversify-config';
import { TYPES } from '../../../src/types';
import UserController from '../../../src/controllers/userController';
import {
  setLoggerMock,
  setConfigMock,
  setHttpContextMock
} from '../../setup/mocks';
import { database } from '../../setup';
import { User } from '../../../src/interfaces';

describe('UserController POST /', () => {
  let loggerMock;

  const createRequest = () => {
    return {
      query: {}
    };
  };

  beforeAll(async () => {
    await database.connect();
  });

  afterAll(async () => {
    await database.disconnect();
  });

  beforeEach(async () => {
    await database.clearAll();
    container.snapshot();

    container
      .bind<UserController>(TYPES.BaseController)
      .to(UserController)
      .inRequestScope();

    loggerMock = setLoggerMock();
    setHttpContextMock(createRequest());
  });

  afterEach(() => {
    container.restore();
  });

  const validUser = {
    username: 'john.doe@aol.com',
    firstName: 'john',
    lastName: 'doe',
    password: 'secret-password'
  };

  const getController = (): UserController => {
    return container.get<UserController>(TYPES.BaseController);
  };

  const copy = (obj): object => JSON.parse(JSON.stringify(obj));

  test('should be able to create a valid user in the database', async () => {
    setConfigMock({
      password_salt: 'this-is-a-secret-salt'
    });

    const userController = getController();
    await userController.post({ body: validUser } as any);
    const response = await userController.get();

    expect(response).toStrictEqual({
      success: true,
      data: [
        {
          firstName: validUser.firstName,
          lastName: validUser.lastName,
          username: validUser.username
        }
      ],
      meta: {
        requestId: undefined
      },
      pagination: {
        page: 0,
        pageSize: 10,
        total: 1
      }
    });
    expect(loggerMock.error).not.toHaveBeenCalled();
  });

  test('user must have a first name', async () => {
    setConfigMock({
      password_salt: 'this-is-a-secret-salt'
    });

    const invalidUser = copy(validUser) as User;
    delete invalidUser.firstName;

    const userController = getController();

    await expect(
      userController.post({ body: invalidUser } as any)
    ).rejects.toThrow(
      'userSchema validation failed: firstName: Path `firstName` is required.'
    );
  });

  test('user must have a last name', async () => {
    setConfigMock({
      password_salt: 'this-is-a-secret-salt'
    });

    const invalidUser = copy(validUser) as User;
    delete invalidUser.lastName;

    const userController = getController();

    await expect(
      userController.post({ body: invalidUser } as any)
    ).rejects.toThrow(
      'userSchema validation failed: lastName: Path `lastName` is required.'
    );
  });

  test('user must have a username', async () => {
    setConfigMock({
      password_salt: 'this-is-a-secret-salt'
    });

    const invalidUser = copy(validUser) as User;
    delete invalidUser.username;

    const userController = getController();

    await expect(
      userController.post({ body: invalidUser } as any)
    ).rejects.toThrow(
      'userSchema validation failed: username: Path `username` is required.'
    );
  });

  test('user must have a password', async () => {
    setConfigMock({
      password_salt: 'this-is-a-secret-salt'
    });

    const invalidUser = copy(validUser) as User;
    delete invalidUser.password;

    const userController = getController();

    await expect(
      userController.post({ body: invalidUser } as any)
    ).rejects.toThrow(
      'userSchema validation failed: password: Path `password` is required.'
    );
  });

  test('user must have username/firstName/lastName/password', async () => {
    setConfigMock({
      password_salt: 'this-is-a-secret-salt'
    });

    const invalidUser = {} as any;

    const userController = getController();

    await expect(userController.post(invalidUser)).rejects.toThrow(
      'userSchema validation failed: lastName: Path `lastName` is required., firstName: Path `firstName` is required., password: Path `password` is required., username: Path `username` is required.'
    );
  });

  test('password salt must be configured', async () => {
    setConfigMock({
      password_salt: undefined
    });

    const userController = getController();

    await expect(
      userController.post({ body: validUser } as any)
    ).rejects.toThrow(
      'Failed to hash (SHA-512) value. The "key" argument must be of type string or an instance of Buffer, TypedArray, DataView, or KeyObject. Received undefined'
    );
  });
});
