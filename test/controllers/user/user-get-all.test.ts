import 'reflect-metadata';
import container from '../../../src/inversify-config';
import { TYPES } from '../../../src/types';
import UserController from '../../../src/controllers/userController';
import {
  setLoggerMock,
  setConfigMock,
  setHttpContextMock
} from '../../setup/mocks';
import { database } from '../../setup';
import { PartialUser } from '../../../src/interfaces';

describe('UserController GET /', () => {
  let loggerMock;

  const createRequest = () => {
    return {
      query: {}
    };
  };

  beforeAll(async () => {
    await database.connect();
  });

  afterAll(async () => {
    await database.disconnect();
  });

  beforeEach(async () => {
    await database.clearAll();
    container.snapshot();

    container
      .bind<UserController>(TYPES.BaseController)
      .to(UserController)
      .inRequestScope();

    loggerMock = setLoggerMock();
  });

  afterEach(() => {
    container.restore();
  });

  const getController = (): UserController => {
    return container.get<UserController>(TYPES.BaseController);
  };

  const createUsers = async numOfUsers => {
    for (let i = 0; i < numOfUsers; i += 1) {
      const user: PartialUser = {
        username: `username-${i}`,
        firstName: `firstName-${i}`,
        lastName: `lastName-${i}`,
        password: `password-${i}`
      };

      const userController = getController();
      // eslint-disable-next-line no-await-in-loop
      await userController.post({ body: user } as any);
    }
  };

  test('should be able to list users in the database even though no user exists', async () => {
    setHttpContextMock(createRequest());
    setConfigMock({
      password_salt: 'this-is-a-secret-salt'
    });

    const userController = getController();
    const response = await userController.get();

    expect(response).toStrictEqual({
      success: true,
      data: [],
      meta: {
        requestId: undefined
      },
      pagination: {
        page: 0,
        pageSize: 10,
        total: 0
      }
    });
    expect(loggerMock.error).not.toHaveBeenCalled();
  });

  test('should be able to fetch list of one user from database', async () => {
    setHttpContextMock(createRequest());
    setConfigMock({
      password_salt: 'this-is-a-secret-salt'
    });

    await createUsers(1);

    const userController = getController();
    const response = await userController.get();

    expect(response).toStrictEqual({
      success: true,
      data: [
        {
          firstName: 'firstName-0',
          lastName: 'lastName-0',
          username: 'username-0'
        }
      ],
      meta: {
        requestId: undefined
      },
      pagination: {
        page: 0,
        pageSize: 10,
        total: 1
      }
    });
    expect(loggerMock.error).not.toHaveBeenCalled();
  });

  test('user password should not be disclosed', async () => {
    setHttpContextMock(createRequest());
    setConfigMock({
      password_salt: 'this-is-a-secret-salt'
    });

    await createUsers(1);

    const userController = getController();
    const response = await userController.get();

    expect(response.data[0]).toHaveProperty('username');
    expect(response.data[0]).not.toHaveProperty('password');
    expect(loggerMock.error).not.toHaveBeenCalled();
  });

  test('pagination should by default only return first 10 users', async () => {
    setHttpContextMock(createRequest());
    setConfigMock({
      password_salt: 'this-is-a-secret-salt'
    });

    await createUsers(15);

    const userController = getController();
    const response = await userController.get();

    expect(response.pagination).toStrictEqual({
      page: 0,
      pageSize: 10,
      total: 15
    });

    expect((response.data as []).length).toBe(10);

    const expectedUsers = [];
    for (let i = 0; i < 10; i += 1) {
      expectedUsers.push({
        username: `username-${i}`,
        firstName: `firstName-${i}`,
        lastName: `lastName-${i}`
      });
    }

    expect(response.data).toStrictEqual(expectedUsers);
    expect(loggerMock.error).not.toHaveBeenCalled();
  });

  test('chainging page size to 5 should only return 5 users from the database', async () => {
    setHttpContextMock({ query: { pageSize: 5 } });
    setConfigMock({
      password_salt: 'this-is-a-secret-salt'
    });

    await createUsers(15);

    const userController = getController();
    const response = await userController.get();

    expect(response.pagination).toStrictEqual({
      page: 0,
      pageSize: 5,
      total: 15
    });

    expect((response.data as []).length).toBe(5);

    const expectedUsers = [];
    for (let i = 0; i < 5; i += 1) {
      expectedUsers.push({
        username: `username-${i}`,
        firstName: `firstName-${i}`,
        lastName: `lastName-${i}`
      });
    }

    expect(response.data).toStrictEqual(expectedUsers);
    expect(loggerMock.error).not.toHaveBeenCalled();
  });

  test('chainging page to 1 and page size to 5 should return next 5 users from the database', async () => {
    setHttpContextMock({ query: { page: 1, pageSize: 5 } });
    setConfigMock({
      password_salt: 'this-is-a-secret-salt'
    });

    await createUsers(15);

    const userController = getController();
    const response = await userController.get();

    expect(response.pagination).toStrictEqual({
      page: 1,
      pageSize: 5,
      total: 15
    });

    expect((response.data as []).length).toBe(5);

    const expectedUsers = [];
    for (let i = 5; i < 10; i += 1) {
      expectedUsers.push({
        username: `username-${i}`,
        firstName: `firstName-${i}`,
        lastName: `lastName-${i}`
      });
    }

    expect(response.data).toStrictEqual(expectedUsers);
    expect(loggerMock.error).not.toHaveBeenCalled();
  });

  test('pagination with page set to less than zero is not allowed', async () => {
    setHttpContextMock({ query: { page: -1 } });
    setConfigMock({
      password_salt: undefined
    });

    const userController = getController();

    await expect(userController.get()).rejects.toThrow(
      'Page value must be greater or equal to 0.'
    );
  });

  test('pagination with page size set to less than one is not allowed', async () => {
    setHttpContextMock({ query: { pageSize: -1 } });
    setConfigMock({
      password_salt: undefined
    });

    const userController = getController();

    await expect(userController.get()).rejects.toThrow(
      'Page size must be greater or equal to 1.'
    );
  });
});
