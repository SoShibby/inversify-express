import 'reflect-metadata';
import { Container } from 'inversify';
import container from '../../../src/inversify-config';
import { TYPES } from '../../../src/types';
import AuthController from '../../../src/controllers/authController';
import UserController from '../../../src/controllers/userController';
import { Credentials } from '../../../src/interfaces';
import {
  setLoggerMock,
  setConfigMock,
  setHttpContextMock
} from '../../setup/mocks';
import { database } from '../../setup';

describe('AuthController POST /login', () => {
  let loggerMock;
  let authControllerContainer: Container;
  let userControllerContainer: Container;
  let loginRequest;

  beforeAll(async () => {
    await database.connect();
  });

  afterAll(async () => {
    await database.disconnect();
  });

  beforeEach(async () => {
    await database.clearAll();
    container.snapshot();

    setHttpContextMock();
    setConfigMock({
      password_salt: 'this-is-a-secret-salt',
      'auth:secret': 'this-is-a-secret-salt'
    });
    loggerMock = setLoggerMock();

    authControllerContainer = container.createChild();
    authControllerContainer
      .bind<AuthController>(TYPES.BaseController)
      .to(AuthController)
      .inRequestScope();

    userControllerContainer = container.createChild();
    userControllerContainer
      .bind<UserController>(TYPES.BaseController)
      .to(UserController)
      .inRequestScope();
  });

  afterEach(() => {
    container.restore();
  });

  const validUser = {
    username: 'john.doe@aol.com',
    firstName: 'john',
    lastName: 'doe',
    password: 'secret-password'
  };

  const getAuthController = (): AuthController => {
    return authControllerContainer.get<AuthController>(TYPES.BaseController);
  };

  const getUserController = (): UserController => {
    return userControllerContainer.get<UserController>(TYPES.BaseController);
  };

  const createUser = async user => {
    const userController = getUserController();
    // eslint-disable-next-line no-await-in-loop
    await userController.post({ body: user } as any);
  };

  const login = async (credentials: Credentials) => {
    const authController = getAuthController();
    loginRequest = { body: credentials, session: { jwt: jest.fn() } } as any;
    return authController.post(loginRequest);
  };

  test('should be able to login with valid credentials', async () => {
    await createUser(validUser);

    const credentials: Credentials = {
      username: validUser.username,
      password: validUser.password
    };

    const response = await login(credentials);

    expect(response).toStrictEqual({
      success: true,
      data: {
        username: validUser.username,
        firstName: validUser.firstName,
        lastName: validUser.lastName
      },
      meta: {
        requestId: undefined
      }
    });
    expect(loggerMock.error).not.toHaveBeenCalled();
    expect(typeof loginRequest.session.jwt).toBe('string');
  });

  test('should not be able to login with invalid username', async () => {
    await createUser(validUser);

    const credentials: Credentials = {
      username: 'invalid-username',
      password: validUser.password
    };

    await expect(login(credentials)).rejects.toThrow(
      'Invalid username and/or password.'
    );
    expect(loggerMock.error).not.toHaveBeenCalled();
    expect(loginRequest.session.jwt).not.toHaveBeenCalled();
  });

  test('should not be able to login with invalid password', async () => {
    await createUser(validUser);

    const credentials: Credentials = {
      username: validUser.username,
      password: 'invalid-password'
    };

    await expect(login(credentials)).rejects.toThrow(
      'Invalid username and/or password.'
    );
    expect(loggerMock.error).not.toHaveBeenCalled();
    expect(loginRequest.session.jwt).not.toHaveBeenCalled();
  });

  test('should not be able to login when providing no credentials', async () => {
    await createUser(validUser);

    const credentials: any = {};

    await expect(login(credentials)).rejects.toThrow(
      'You must provide a username and password to login.'
    );
    expect(loggerMock.error).not.toHaveBeenCalled();
    expect(loginRequest.session.jwt).not.toHaveBeenCalled();
  });
});
