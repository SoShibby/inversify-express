import 'reflect-metadata';
import container from '../../../src/inversify-config';
import { TYPES } from '../../../src/types';
import SecretController from '../../../src/controllers/secretController';
import { setConfigMock, setHttpContextMock } from '../../setup/mocks';

describe('SecretController GET /', () => {
  beforeEach(() => {
    container.snapshot();

    setConfigMock({});
    setHttpContextMock();

    container
      .bind<SecretController>(TYPES.BaseController)
      .to(SecretController)
      .inRequestScope();
  });

  afterEach(() => {
    container.restore();
  });

  const getController = (): SecretController => {
    return container.get<SecretController>(TYPES.BaseController);
  };

  test('get request on secret controller should return valid response', async () => {
    const secretController = getController();
    const response = await secretController.get();

    expect(response).toStrictEqual({
      success: true,
      data: {
        message: 'You can access this secret resource because you are logged in'
      },
      meta: {
        requestId: undefined
      }
    });
  });
});
