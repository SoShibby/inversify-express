import 'reflect-metadata';
import { JsonWebTokenError } from 'jsonwebtoken';
import * as Cookies from 'cookies';
import * as Keygrip from 'keygrip';
import { Config, Logger, User } from '../../src/interfaces';
import { AuthProvider } from '../../src/auth';
import { TYPES } from '../../src/types';
import container from '../../src/inversify-config';
import { JWTUtil } from '../../src/utils';

const { Cookie } = Cookies;

describe('AuthProvider', () => {
  const validCookieName = 'session.cookie';
  const validJWTSecret = 'this-is-a-valid-jwt-secret';
  const validCookieKey1 = 'valid-cookie-key-1';
  const validCookieKey2 = 'valid-cookie-key-2';
  const validUser: User = {
    _id: '5099803df3f4948bd2f98391',
    username: 'test-user',
    firstName: 'john',
    lastName: 'doe',
    password:
      'cd9dc05e5ac4efc6c9dc84d9bae79e01d8c48299908f7b477f7803941a0ef68259c7a9af7ffe4427e6bb8494915bcbe3205e9b66a286d8f81887c3726a5078bb'
  };
  let logMock;

  const createLogMock = (): Logger => {
    logMock = {
      info: jest.fn(),
      debug: jest.fn(),
      warn: jest.fn(),
      error: jest.fn(),
      log: jest.fn()
    };

    return logMock;
  };

  const setConfig = (config): void => {
    const configMock: Config = {
      get: name => {
        return config[name];
      },
      prettyPrint: (): string => {
        return '';
      }
    };

    container.bind<Config>(TYPES.Config).toConstantValue(configMock);
  };

  const createRequest = (cookies?: Cookies.Cookie[]): object => {
    const req = {
      protocol: 'https',
      headers: {}
    };

    if (cookies) {
      const cookieStringArray = cookies.map(cookie => cookie.toString());
      const cookieStr = cookieStringArray.join(';');

      req.headers = {
        cookie: cookieStr
      };
    }

    return req;
  };

  const createResponse = () => {
    return {
      getHeader: () => {
        return undefined;
      },
      setHeader: jest.fn()
    };
  };

  const asBase64 = str => {
    return Buffer.from(str).toString('base64');
  };

  const createCookie = ({ user, jwtSecret, cookieName }): Cookies.Cookie => {
    const jwt = JWTUtil.createJWT(user, jwtSecret);
    const encodedJwt = asBase64(JSON.stringify({ jwt }));
    return new Cookie(cookieName, encodedJwt);
  };

  const createSignedCookie = (cookie, keys: string[]) => {
    const name = `${cookie.name}.sig`;
    const value = new Keygrip(keys).sign(cookie.toString());
    return new Cookie(name, value);
  };

  const createAuthCookies = ({
    user,
    jwtSecret,
    cookieName,
    cookieKeys
  }): Cookies.Cookie => {
    const cookie = createCookie({ user, jwtSecret, cookieName });
    const signedCookie = createSignedCookie(cookie, cookieKeys);

    return [cookie, signedCookie];
  };

  const authenticate = async (req, res) => {
    container.bind<AuthProvider>(TYPES.AuthProvider).to(AuthProvider);
    const authProvider = container.get<AuthProvider>(TYPES.AuthProvider);
    return authProvider.getUser(req, res);
  };

  beforeEach(() => {
    container.snapshot();
    container.unbind(TYPES.Config);
    container.unbind(TYPES.GlobalLogger);
    container.bind<Logger>(TYPES.GlobalLogger).toConstantValue(createLogMock());
  });

  afterEach(() => {
    container.restore();
  });

  test('user should be authenticated when providing a valid cookie', async () => {
    setConfig({
      'auth:secret': validJWTSecret,
      'auth:session_key_1': validCookieKey1,
      'auth:session_key_2': validCookieKey2
    });
    const cookies = createAuthCookies({
      user: validUser,
      jwtSecret: validJWTSecret,
      cookieName: validCookieName,
      cookieKeys: [validCookieKey1, validCookieKey2]
    });

    const req = createRequest(cookies);
    const res = createResponse();

    const principal = await authenticate(req, res);
    expect(await principal.isAuthenticated()).toBe(true);
    expect(principal.details).toStrictEqual({
      _id: validUser._id
    });
    expect(logMock.info).not.toHaveBeenCalled();
    expect(logMock.error).not.toHaveBeenCalled();
    expect(res.setHeader).not.toHaveBeenCalled();
  });

  test("user should not be authenticated when user don't provide a cookie", async () => {
    setConfig({});
    const req = createRequest();
    const res = createResponse();
    const principal = await authenticate(req, res);

    expect(await principal.isAuthenticated()).toBe(false);
    expect(principal.details).toBe(null);
    expect(logMock.info).not.toHaveBeenCalled();
    expect(logMock.error).not.toHaveBeenCalled();
  });

  test('user should not be authenticated when providing invalid jwt secret', async () => {
    setConfig({
      'auth:secret': validJWTSecret,
      'auth:session_key_1': validCookieKey1,
      'auth:session_key_2': validCookieKey2
    });

    const cookies = createAuthCookies({
      user: validUser,
      jwtSecret: 'this-jwt-secret-is-invalid',
      cookieName: validCookieName,
      cookieKeys: [validCookieKey1, validCookieKey2]
    });

    const req = createRequest(cookies);
    const res = createResponse();
    const principal = await authenticate(req, res);

    expect(await principal.isAuthenticated()).toBe(false);
    expect(principal.details).toBe(null);
    expect(logMock.info).not.toHaveBeenCalled();
    expect(logMock.error).toHaveBeenCalledTimes(1);
    expect(logMock.error).toBeCalledWith(
      new JsonWebTokenError('invalid signature')
    );
    expect(res.setHeader).not.toHaveBeenCalled();
  });

  test('user should not be authenticated when user provider cookie with the wrong name', async () => {
    setConfig({
      'auth:secret': validJWTSecret,
      'auth:session_key_1': validCookieKey1,
      'auth:session_key_2': validCookieKey2
    });

    const cookies = createAuthCookies({
      user: validUser,
      jwtSecret: validJWTSecret,
      cookieName: 'this-cookie-name-is-not-correct',
      cookieKeys: [validCookieKey1, validCookieKey2]
    });

    const req = createRequest(cookies);
    const res = createResponse();
    const principal = await authenticate(req, res);

    expect(await principal.isAuthenticated()).toBe(false);
    expect(principal.details).toBe(null);
    expect(logMock.info).not.toHaveBeenCalled();
    expect(logMock.error).not.toHaveBeenCalled();
    expect(res.setHeader).not.toHaveBeenCalled();
  });

  test('user should not be authenticated when cookie is signed with wrong keys', async () => {
    setConfig({
      'auth:secret': validJWTSecret,
      'auth:session_key_1': validCookieKey1,
      'auth:session_key_2': validCookieKey2
    });

    const cookies = createAuthCookies({
      user: validUser,
      jwtSecret: validJWTSecret,
      cookieName: validCookieName,
      cookieKeys: ['invalid-cookie-key-1', 'invalid-cookie-key-2']
    });

    const req = createRequest(cookies);
    const res = createResponse();
    const principal = await authenticate(req, res);

    expect(await principal.isAuthenticated()).toBe(false);
    expect(principal.details).toBe(null);
    expect(logMock.info).not.toHaveBeenCalled();
    expect(logMock.error).not.toHaveBeenCalled();
    expect(res.setHeader).toHaveBeenCalledWith('Set-Cookie', [
      'session.cookie.sig=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT; secure; httponly'
    ]);
  });

  test('user should not be authenticated when cookie is signed where the first cookie key is invalid and second key is valid', async () => {
    setConfig({
      'auth:secret': validJWTSecret,
      'auth:session_key_1': validCookieKey1,
      'auth:session_key_2': validCookieKey2
    });

    const cookies = createAuthCookies({
      user: validUser,
      jwtSecret: validJWTSecret,
      cookieName: validCookieName,
      cookieKeys: ['invalid-cookie-key-1', validCookieKey2]
    });

    const req = createRequest(cookies);
    const res = createResponse();
    const principal = await authenticate(req, res);

    expect(await principal.isAuthenticated()).toBe(false);
    expect(principal.details).toBe(null);
    expect(logMock.info).not.toHaveBeenCalled();
    expect(logMock.error).not.toHaveBeenCalled();
    expect(res.setHeader).toHaveBeenCalledWith('Set-Cookie', [
      'session.cookie.sig=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT; secure; httponly'
    ]);
  });

  // Only the first key is used for signing the cookie.
  test('user should be authenticated when cookie is signed where the first cookie key is valid and second key is invalid', async () => {
    setConfig({
      'auth:secret': validJWTSecret,
      'auth:session_key_1': validCookieKey1,
      'auth:session_key_2': validCookieKey2
    });

    const cookies = createAuthCookies({
      user: validUser,
      jwtSecret: validJWTSecret,
      cookieName: validCookieName,
      cookieKeys: [validCookieKey1, 'invalid-cookie-key-2']
    });

    const req = createRequest(cookies);
    const res = createResponse();
    const principal = await authenticate(req, res);

    expect(await principal.isAuthenticated()).toBe(true);
    expect(principal.details).toStrictEqual({
      _id: validUser._id
    });
    expect(logMock.info).not.toHaveBeenCalled();
    expect(logMock.error).not.toHaveBeenCalled();
    expect(res.setHeader).not.toHaveBeenCalled();
  });

  test('user should not be authenticated when cookie is not signed', async () => {
    setConfig({
      'auth:secret': validJWTSecret,
      'auth:session_key_1': validCookieKey1,
      'auth:session_key_2': validCookieKey2
    });

    const cookie = createCookie({
      user: validUser,
      jwtSecret: validJWTSecret,
      cookieName: validCookieName
    });

    const req = createRequest([cookie]);
    const res = createResponse();
    const principal = await authenticate(req, res);

    expect(await principal.isAuthenticated()).toBe(false);
    expect(principal.details).toBe(null);
    expect(logMock.info).not.toHaveBeenCalled();
    expect(logMock.error).not.toHaveBeenCalled();
    expect(res.setHeader).not.toHaveBeenCalled();
  });

  test('no jwt secret configured should throw error', async () => {
    setConfig({
      'auth:secret': undefined,
      'auth:session_key_1': validCookieKey1,
      'auth:session_key_2': validCookieKey2
    });

    const cookies = createAuthCookies({
      user: validUser,
      jwtSecret: validJWTSecret,
      cookieName: validCookieName,
      cookieKeys: [validCookieKey1, validCookieKey2]
    });

    const req = createRequest(cookies);
    const res = createResponse();
    const principal = await authenticate(req, res);

    expect(await principal.isAuthenticated()).toBe(false);
    expect(principal.details).toBe(null);
    expect(logMock.info).not.toHaveBeenCalled();
    expect(logMock.error).toBeCalledWith(
      new JsonWebTokenError('secret or public key must be provided')
    );
    expect(res.setHeader).not.toHaveBeenCalled();
  });

  test('no auth keys configured should throw error', async () => {
    setConfig({
      'auth:secret': validJWTSecret,
      'auth:session_key_1': undefined,
      'auth:session_key_2': undefined
    });

    const cookies = createAuthCookies({
      user: validUser,
      jwtSecret: validJWTSecret,
      cookieName: validCookieName,
      cookieKeys: [validCookieKey1, validCookieKey2]
    });

    const req = createRequest(cookies);
    const res = createResponse();
    const principal = await authenticate(req, res);

    expect(await principal.isAuthenticated()).toBe(false);
    expect(principal.details).toBe(null);
    expect(logMock.info).not.toHaveBeenCalled();
    expect(logMock.error).toHaveBeenCalledTimes(1);
    expect(res.setHeader).not.toHaveBeenCalled();
  });
});
