declare namespace Express {
  export interface Request {
    startTime?: Date;
    session: { jwt: string };
  }
}
