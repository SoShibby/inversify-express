import * as rTracer from 'cls-rtracer';
import { ApiResponse } from '../interfaces';
import { Pagination } from '../../core/interfaces';

class ApiResponseUtil {
  public static createResponseBody({
    data,
    success = true,
    pagination
  }: {
    data: {};
    success?: boolean;
    pagination?: Pagination;
  }): ApiResponse {
    return {
      success,
      meta: {
        requestId: rTracer.id()
      },
      data,
      ...(pagination && { pagination })
    };
  }
}

export default ApiResponseUtil;
