import { sign, decode } from 'jsonwebtoken';
import { JWT } from '../interfaces';
import { User } from '../../core/interfaces';

export default {
  createJWT: (user: User, secret: string): string => {
    // We want to make sure we don't store any sensitive information in the JWT as it is exposed to the user.
    // So if an attacker hacks the user and comes over the JWT he/she can read the content of it.
    const now = new Date();
    const jwtContent: JWT = {
      exp: new Date(now.getTime() + 24 * 60 * 60 * 1000).getTime(), // 24 hours.
      user: {
        _id: user._id
      }
    };

    return sign(jwtContent, secret);
  },
  parseJWT(jwtStr: string): JWT {
    const jwtContent = decode(jwtStr);

    if (typeof jwtContent !== 'object') {
      throw new Error(
        `Expected jwt to be of type object, got ${typeof jwtContent}.`
      );
    }

    if (!jwtContent.user || !jwtContent.user._id) {
      throw new Error("Couldn't find '_id' property on user in jwt.");
    }

    if (typeof jwtContent.user._id !== 'string') {
      throw new Error(
        `Expected _id to be of type string on user, got ${typeof jwtContent.user
          ._id}`
      );
    }

    if (!jwtContent.exp) {
      throw new Error("Couldn't find 'exp' property in jwt.");
    }

    if (typeof jwtContent.exp !== 'number') {
      throw new Error(
        `Expected exp to be of type number on user, got ${typeof jwtContent.exp}`
      );
    }

    return {
      exp: jwtContent.exp,
      user: {
        _id: jwtContent.user._id
      }
    };
  }
};
