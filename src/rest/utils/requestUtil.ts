import * as express from 'express';
import StringUtil from '../../core/utils/stringUtil';
import { RequestLogContext } from '../../common/interfaces';

const { isString, asString, truncateString } = StringUtil;
const LOG_MAX_BODY_LENGTH = 3000;
const SENSITIVE_REQUEST_PATHS = {
  POST: new Set([
    // /v1/users request body contains user password.
    '/v1/auth/login',
    // /v1/users request body constains user password.
    '/v1/users'
  ])
};

class RequestUtil {
  public static getLogContext(req: express.Request): RequestLogContext {
    return {
      method: req.method,
      url: req.originalUrl,
      body: this.getRequestBody(req)
    };
  }

  private static isSensitiveRequestPath(req): boolean {
    return SENSITIVE_REQUEST_PATHS[req.method]?.has(req.originalUrl);
  }

  private static getRequestBody(req): string {
    if (this.isSensitiveRequestPath(req)) {
      return '[redacted because of sensitive request body]';
    }

    const body = isString(req.body) ? req.body : asString(req.body);

    // Truncate request body if it is too long,
    // we don't want to risk that a malicious user filling our logs with
    // lots of dummy data.
    return truncateString(body, LOG_MAX_BODY_LENGTH);
  }
}

export default RequestUtil;
