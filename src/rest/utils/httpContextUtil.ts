export default {
  getUserId: (httpContext): string => {
    return httpContext?.user?.details?._id;
  }
};
