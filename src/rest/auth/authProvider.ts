import { injectable, inject } from 'inversify';
import * as express from 'express';
import { interfaces } from 'inversify-express-utils';
import * as Cookies from 'cookies';
import { verify, TokenExpiredError } from 'jsonwebtoken';
import Principal from './principal';
import { Config, Logger } from '../../common/interfaces';
import { TYPES } from '../../types';
import { JWTUtil } from '../utils';

@injectable()
class AuthProvider implements interfaces.AuthProvider {
  @inject(TYPES.Config) private config: Config;

  @inject(TYPES.GlobalLogger) private log: Logger;

  private COOKIE_NAME = 'session.cookie';

  public async getUser(
    req: express.Request,
    res: express.Response
  ): Promise<interfaces.Principal> {
    let jwt;

    try {
      jwt = this.getJwtTokenFromCookie(req, res);
    } catch (e) {
      // This could happen if the user sends an invalid cookie.
      // If that is the case then log the error (in case this is a malicious user and we need to investigate this further).
      this.log.error(e);
    }

    if (!jwt) {
      return new Principal(null);
    }

    try {
      this.verifyJwt(jwt);
      return this.getPrincipalFromJwt(jwt);
    } catch (e) {
      if (e instanceof TokenExpiredError) {
        this.log.info(e);
      } else {
        this.log.error(e);
      }

      return new Principal(null);
    }
  }

  private getJwtTokenFromCookie(req, res): string {
    const keys = [
      this.config.get('auth:session_key_1'),
      this.config.get('auth:session_key_2')
    ];

    const cookies = new Cookies(req, res, {
      keys
    });

    const cookie = cookies.get(this.COOKIE_NAME, { signed: true });

    if (!cookie) {
      return undefined;
    }

    const decoded = this.decode(cookie);
    return decoded.jwt;
  }

  private verifyJwt(jwt): void {
    const authSecret = this.config.get('auth:secret');
    verify(jwt, authSecret);
  }

  private getPrincipalFromJwt(jwt: string): Principal {
    const jwtContent = JWTUtil.parseJWT(jwt);
    return new Principal(jwtContent.user);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private decode(str): Record<string, any> {
    const body = Buffer.from(str, 'base64').toString('utf8');
    return JSON.parse(body);
  }
}

export default AuthProvider;
