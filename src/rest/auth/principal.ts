import { interfaces } from 'inversify-express-utils';
import { JWTUser } from '../interfaces';

class Principal implements interfaces.Principal {
  public details: JWTUser;

  public constructor(details: JWTUser) {
    this.details = details;
  }

  public isAuthenticated(): Promise<boolean> {
    return Promise.resolve(this.details !== null);
  }

  public isResourceOwner(): Promise<boolean> {
    throw new Error('isResourceOwner is not implemented.');
  }

  public isInRole(): Promise<boolean> {
    throw new Error('isInRole is not implemented.');
  }
}

export default Principal;
