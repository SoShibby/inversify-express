import { Container } from 'inversify';

export default interface Server {
  start(container: Container): void;
}
