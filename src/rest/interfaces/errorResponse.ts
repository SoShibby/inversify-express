import { ResponseCode } from '../enums';

export default interface ErrorResponse {
  message: string;
  statusCode: ResponseCode;
}
