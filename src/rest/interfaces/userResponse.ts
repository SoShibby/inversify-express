export default interface UserResponse {
  username;
  firstName;
  lastName;
}
