import Pagination from './pagination';

export default interface ApiResponse {
  success: boolean;
  data: {} | [];
  meta: {
    requestId: string;
  };
  pagination?: Pagination;
}
