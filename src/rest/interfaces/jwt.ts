import JWTUser from './jwtUser';

export default interface JWT {
  exp: number;
  user: JWTUser;
}
