import { SecretResponse, ApiResponse } from '../../interfaces';
import { ResponseUtil } from '../../utils';

const { createResponseBody } = ResponseUtil;

class UserResponseTransformer {
  public static transformSecret(message: string): ApiResponse {
    const data = UserResponseTransformer.toExternalFormat(message);
    return createResponseBody({ data });
  }

  private static toExternalFormat(message: string): SecretResponse {
    return {
      message
    };
  }
}

export default UserResponseTransformer;
