import {
  UserResponse,
  ApiResponse
} from '../../interfaces';
import {
  User,
  PaginatedUsers
} from '../../../core/interfaces';
import { ResponseUtil } from '../../utils';

const { createResponseBody } = ResponseUtil;

class UserResponseTransformer {
  public static transformUser(user: User): ApiResponse {
    const data = UserResponseTransformer.toExternalFormat(user);
    return createResponseBody({ data });
  }

  public static transformPaginatedUsers(
    paginatedUsers: PaginatedUsers
  ): ApiResponse {
    const data = paginatedUsers.items.map(user =>
      UserResponseTransformer.toExternalFormat(user)
    );

    return createResponseBody({
      data,
      pagination: paginatedUsers.pagination
    });
  }

  private static toExternalFormat(user: User): UserResponse {
    const { username, firstName, lastName } = user;

    return {
      username,
      firstName,
      lastName
    };
  }
}

export default UserResponseTransformer;
