import { UserError } from '../../../core/errors';

export default class RequestParamTransformer {
  private DONE = 1;

  private name: string;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private value: any;

  private validations: (() => number | void)[];

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(name: string, value: any) {
    this.name = name;
    this.value = value;
    this.validations = [];
  }

  private chain(func: () => number | void): RequestParamTransformer {
    this.validations.push(func);
    return this;
  }

  public optional(): RequestParamTransformer {
    // eslint-disable-next-line consistent-return
    return this.chain(() => {
      if (this.value === undefined) {
        return this.DONE;
      }
    });
  }

  public number(): RequestParamTransformer {
    return this.chain(() => {
      this.valueIsRequired();
      this.valueIsNumber();
      this.value = Number(this.value);
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public get(): any {
    try {
      // eslint-disable-next-line no-restricted-syntax
      for (const validation of this.validations) {
        if (validation() === this.DONE) {
          return this.value;
        }
      }
    } catch (err) {
      if (err instanceof UserError) {
        throw new UserError(
          `Parameter '${this.name}' is invalid. ${err.message}`,
          err
        );
      }

      throw err;
    }

    return this.value;
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  private valueIsRequired() {
    if (this.value === undefined || this.value === null) {
      throw new UserError('Parameter is required.');
    }
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  private valueIsNumber() {
    if (Number.isNaN(Number(this.value))) {
      throw new UserError('Parameter may only contain digits.');
    }
  }
}
