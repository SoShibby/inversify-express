import * as Prometheus from 'prom-client';
import * as express from 'express';
import { controller, httpGet, response } from 'inversify-express-utils';
import BaseController from '../common/baseController';

@controller('')
export class MetricsController extends BaseController {
  @httpGet('/metrics')
  public get(@response() res: express.Response): string {
    res.set('Content-Type', Prometheus.register.contentType);
    return Prometheus.register.metrics();
  }
}
