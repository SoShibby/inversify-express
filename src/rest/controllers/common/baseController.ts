import { BaseHttpController } from 'inversify-express-utils';
import * as jf from 'joiful';
import RequestParamTransformer from '../../transformers/request/requestParamTransformer';
import { UserError } from '../../../core/errors';

export interface Constructor<T> {
  new (...args: any[]): T;
}

export default abstract class UserController extends BaseHttpController {
  protected query(name: string): RequestParamTransformer {
    const value = this.httpContext.request.query[name];
    return new RequestParamTransformer(name, value);
  }

  protected paramsAs(Class: Constructor<any>): any {
    return this.validate(this.httpContext.request.params, Class);
  }

  protected queryAs<
    TClass extends Constructor<any>,
    TInstance = TClass extends Constructor<infer TInstance> ? TInstance : never
  >(Class: Constructor<any>): TInstance {
    return this.validate(this.httpContext.request.query, Class);
  }

  protected bodyAs<
    TClass extends Constructor<any>,
    TInstance = TClass extends Constructor<infer TInstance> ? TInstance : never
  >(Class: Constructor<any>): TInstance {
    return this.validate(this.httpContext.request.body, Class);
  }

  validate<
    TClass extends Constructor<any>,
    TInstance = TClass extends Constructor<infer TInstance> ? TInstance : never
  >(target: Partial<TInstance>, Class: Constructor<any>): TInstance {
    const result = jf.validateAsClass(target, Class, { stripUnknown: true });

    if (result.error) {
      const messages = result.error.details
        .map(detail => detail.message)
        .join(', ');

      throw new UserError(`Validation failed. ${messages}.`);
    }

    return result.value;
  }
}
