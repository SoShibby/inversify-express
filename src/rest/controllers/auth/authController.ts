import * as express from 'express';
import { inject } from 'inversify';
import {
  controller,
  request,
  httpPost,
  httpGet
} from 'inversify-express-utils';
import { LoginService, User } from '../../../core/interfaces';
import { Config } from '../../../common/interfaces';
import { ApiResponse } from '../../interfaces';
import { CredentialsBody } from './types/body';
import { TYPES } from '../../../types';
import { UserResponseTransformer } from '../../transformers/response';
import { JWTUtil } from '../../utils';
import BaseController from '../common/baseController';

const { transformUser } = UserResponseTransformer;

@controller('/v1/auth', TYPES.RequestLoggerMiddleware)
export default class LoginController extends BaseController {
  @inject(TYPES.LoginService) private loginService: LoginService;

  @inject(TYPES.Config) private config: Config;

  @httpPost('/login')
  public async post(@request() req: express.Request): Promise<ApiResponse> {
    const credentials: CredentialsBody = this.bodyAs(CredentialsBody);

    const user: User = await this.loginService.login(credentials);

    const authSecret = this.config.get('auth:secret');
    req.session.jwt = JWTUtil.createJWT(user, authSecret);

    return transformUser(user);
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  @httpGet('/logout')
  public get(@request() req: express.Request) {
    req.session = null;
  }
}
