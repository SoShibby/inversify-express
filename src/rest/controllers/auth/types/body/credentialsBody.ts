import * as jf from 'joiful';

export default class CredentialsBody {
  @(jf.string().required())
  username: string;

  @(jf.string().required())
  password: string;
}
