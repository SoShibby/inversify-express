import { HealthChecker, HealthStatus } from '@cloudnative/health';
import { controller, httpGet } from 'inversify-express-utils';
import BaseController from '../common/baseController';

@controller('')
export class HealthController extends BaseController {
  private healthcheck: HealthChecker;

  constructor() {
    super();
    this.healthcheck = new HealthChecker();
  }

  @httpGet('/health')
  public async getHealth(): Promise<HealthStatus> {
    return this.healthcheck.getStatus();
  }

  @httpGet('/liveness')
  public async getLiveness(): Promise<HealthStatus> {
    return this.healthcheck.getLivenessStatus();
  }

  @httpGet('/readiness')
  public async getReadiness(): Promise<HealthStatus> {
    return this.healthcheck.getReadinessStatus();
  }
}
