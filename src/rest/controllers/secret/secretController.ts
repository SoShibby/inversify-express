import { controller, httpGet } from 'inversify-express-utils';
import { TYPES } from '../../../types';
import { ApiResponse } from '../../interfaces';
import { SecretResponseTransformer } from '../../transformers/response';
import BaseController from '../common/baseController';

const { transformSecret } = SecretResponseTransformer;

@controller('/v1', TYPES.RequestLoggerMiddleware, TYPES.AuthenticatedMiddleware)
export default class SecretController extends BaseController {
  @httpGet('/secret')
  public get(): ApiResponse {
    return transformSecret(
      'You can access this secret resource because you are logged in'
    );
  }
}
