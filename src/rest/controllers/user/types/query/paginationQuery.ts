import * as jf from 'joiful';

export default class PaginationQuery {
  @(jf.number())
  page: number;

  @(jf.number())
  pageSize?;
}
