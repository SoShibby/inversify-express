import * as jf from 'joiful';

export default class CreateUserBody {
  @(jf.string().required())
  username: string;

  @(jf.string().required())
  password?;

  @(jf.string().required())
  firstName?;

  @(jf.string().required())
  lastName?;
}
