import { inject } from 'inversify';
import { controller, httpGet, httpPost } from 'inversify-express-utils';
import { ApiResponse } from '../../interfaces';
import { UserService } from '../../../core/interfaces';
import { TYPES } from '../../../types';
import { UserResponseTransformer } from '../../transformers/response';
import BaseController from '../common/baseController';
import { CreateUserBody } from './types/body';
import { PaginationQuery } from './types/query';

const { transformPaginatedUsers, transformUser } = UserResponseTransformer;

@controller('/v1', TYPES.RequestLoggerMiddleware)
export default class UserController extends BaseController {
  @inject(TYPES.UserService) private userService: UserService;

  @httpGet('/users')
  public async get(): Promise<ApiResponse> {
    const pagination: PaginationQuery = this.queryAs(PaginationQuery);

    const paginatedUsers = await this.userService.getAllUsers(
      pagination.page,
      pagination.pageSize
    );

    return transformPaginatedUsers(paginatedUsers);
  }

  @httpPost('/users')
  public async post(): Promise<ApiResponse> {
    const user: CreateUserBody = this.bodyAs(CreateUserBody);

    const addedUser = await this.userService.addUser(user);

    return transformUser(addedUser);
  }
}
