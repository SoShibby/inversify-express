import * as express from 'express';
import { inject, injectable, Container } from 'inversify';
import { getRouteInfo, InversifyExpressServer } from 'inversify-express-utils';
import * as prettyjson from 'prettyjson';
import * as middlewares from './middlewares';
import './controllers';
import { TYPES } from '../types';
import { Logger, Config } from '../common/interfaces';
import { Server } from './interfaces';
import { AuthProvider } from './auth';

@injectable()
export default class ServerImpl implements Server {
  @inject(TYPES.GlobalLogger) private log: Logger;

  @inject(TYPES.Config) private config: Config;

  public start(container: Container): void {
    const port: number = this.config.get('port');
    const server = new InversifyExpressServer(
      container,
      null,
      null,
      null,
      AuthProvider
    );

    server
      .setConfig((app: express.Application) => {
        // Cookie session is used for managing cookies.
        middlewares.cookieSession(app, this.config);

        // Secure application by setting various http headers.
        middlewares.helmet(app);

        // Add request id to all incoming http requests.
        middlewares.requestId(app);

        // Add cors header to limit who can call the api service.
        middlewares.cors(app, this.config);

        // Collect metrics from express (such as duration and response codes).
        middlewares.prometheus(app);

        // Parse request body strings as json if Content-Type header is set to application/json.
        middlewares.bodyParser(app);

        // Handle any error thrown by the body parser.
        // So we can send back a better error message to the user.
        middlewares.bodyParserErrorHandler(app, this.log);
      })
      .setErrorConfig((app: express.Application) => {
        // Handle any error that is thrown in a route and send back an error message to the user.
        middlewares.rootErrorHandler(app, this.log);
      })
      .build()
      .listen(port);

    // Print all routes to the console.
    this.log.info(`API Routes: ${this.getRoutes(container)}`);

    // Print how to access the application.
    this.log.info(`Application served at http://localhost:${port}`);
  }

  private getRoutes(container): string {
    if (process.env.NODE_ENV === 'production') {
      return JSON.stringify(
        {
          routes: getRouteInfo(container)
        },
        null,
        2
      );
    }

    return prettyjson.render({
      routes: getRouteInfo(container)
    });
  }
}
