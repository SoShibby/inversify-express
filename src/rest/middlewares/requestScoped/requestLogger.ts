import * as express from 'express';
import { BaseMiddleware } from 'inversify-express-utils';
import { injectable, inject } from 'inversify';
import { Logger, LogContext } from '../../../common/interfaces';
import { TYPES } from '../../../types';
import { NumberUtil } from '../../../core/utils';
import { HttpContextUtil, RequestUtil } from '../../utils';
import { LogType } from '../../../common/enums';

@injectable()
class RequestLoggerMiddleware extends BaseMiddleware {
  constructor(@inject(TYPES.Logger) private log: Logger) {
    super();
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  public handler(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const requestLogContext = RequestUtil.getLogContext(req);
    this.logRequest(req, requestLogContext);

    req.startTime = new Date();

    // Proxy the real 'end' function
    const originalResEnd = res.end;

    // eslint-disable-next-line @typescript-eslint/no-unused-vars,@typescript-eslint/no-explicit-any
    res.end = (chunk?: any, encodingOrCb?: any, cb?: any): void => {
      // Restore the original res.end function.
      res.end = originalResEnd;
      res.end(chunk, encodingOrCb, cb);
      this.logResponse(req, res, requestLogContext);
    };

    next();
  }

  private logRequest(req, requestLogContext): void {
    const userAgent = req.headers['user-agent'];
    const ip = this.getIP(req);

    const message = `Request ${req.method} ${req.originalUrl} ${userAgent} ${ip}`;
    const context: LogContext = {
      user: {
        id: HttpContextUtil.getUserId(this.httpContext)
      },
      request: requestLogContext,
      type: LogType.REQUEST
    };

    this.log.info(message, context);
  }

  private logResponse(req, res, requestLogContext): void {
    const start = req.startTime.getTime();
    const end = new Date().getTime();
    const responseTime: number = end - start;
    const contentLength = NumberUtil.parseNumber(
      res.getHeader('content-length')
    );

    const message = `Response ${res.statusCode} ${responseTime} ms ${contentLength} bytes`;
    const context: LogContext = {
      user: {
        id: HttpContextUtil.getUserId(this.httpContext)
      },
      request: requestLogContext,
      response: {
        statusCode: res.statusCode,
        responseTime,
        contentLength
      },
      type: LogType.RESPONSE
    };

    this.log.info(message, context);
  }

  private getIP(req): string {
    return (
      req.ip ||
      // eslint-disable-next-line no-underscore-dangle
      req._remoteAddress ||
      (req.connection && req.connection.remoteAddress) ||
      undefined
    );
  }
}

export default RequestLoggerMiddleware;
