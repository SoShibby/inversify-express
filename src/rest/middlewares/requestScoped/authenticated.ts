import * as express from 'express';
import { BaseMiddleware } from 'inversify-express-utils';
import { injectable } from 'inversify';
import { UnauthorizedError } from '../../../core/errors';

@injectable()
class AuthenticatedMiddleware extends BaseMiddleware {
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  public async handler(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    if (await this.httpContext.user.isAuthenticated()) {
      next();
    } else {
      next(
        new UnauthorizedError('You must be logged in to access this resource.')
      );
    }
  }
}

export default AuthenticatedMiddleware;
