import * as express from 'express';
import * as cors from 'cors';
import { Config } from '../../../common/interfaces';

export default (app: express.Application, config: Config): void => {
  app.use(
    cors({
      origin: config.get('cors:origin'),
      optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
    })
  );
};
