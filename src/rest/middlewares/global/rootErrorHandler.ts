import * as express from 'express';
import { ApiResponse, ErrorResponse } from '../../interfaces';
import { Logger, LogContext } from '../../../common/interfaces';
import { UserError, UnauthorizedError } from '../../../core/errors';
import { HttpContextUtil, RequestUtil, ResponseUtil } from '../../utils';
import { LogType } from '../../../common/enums';
import { ResponseCode } from '../../enums';

const HTTP_CONTEXT_METADATA_KEY = 'inversify-express-utils:httpcontext';

const getLogContext = (req, statusCode: number): LogContext => {
  const httpContext = Reflect.getMetadata(HTTP_CONTEXT_METADATA_KEY, req);
  const userId = HttpContextUtil.getUserId(httpContext);
  const requestLogContext = RequestUtil.getLogContext(req);

  return {
    user: {
      id: userId
    },
    request: requestLogContext,
    response: {
      statusCode
    },
    type: LogType.ERROR_BOUNDARY
  };
};

const isUserError = (error): boolean => {
  return error instanceof UserError;
};

const isUnauthorizedError = (error): boolean => {
  return error instanceof UnauthorizedError;
};

export default (app: express.Application, logger: Logger): void => {
  // NOTE: The 'next' variable is unused, but this function won't be called if we don't have it.
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  app.use((err, req, res, next) => {
    let message;
    let statusCode;
    let logLevel;

    if (isUserError(err)) {
      message = err.message;
      statusCode = ResponseCode.BAD_REQUEST;
      logLevel = 'info';
    } else if (isUnauthorizedError(err)) {
      message = err.message;
      statusCode = ResponseCode.UNAUTHORIZED;
      logLevel = 'info';
    } else {
      // Be careful not to disclose any internal information to the user.
      // As we don't know what kind of error this is.
      message = 'An unexpected error occurred';
      statusCode = ResponseCode.INTERNAL_SERVER_ERROR;
      logLevel = 'error';
    }

    const context = getLogContext(req, statusCode);
    logger.log(logLevel, err, context);

    const error: ErrorResponse = {
      message,
      statusCode
    };

    const response: ApiResponse = ResponseUtil.createResponseBody({
      data: error,
      success: false
    });

    res.status(statusCode).send(response);
  });
};
