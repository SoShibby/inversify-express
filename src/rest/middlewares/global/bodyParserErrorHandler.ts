import * as express from 'express';
import { ApiResponse, ErrorResponse } from '../../interfaces';
import { Logger } from '../../../common/interfaces';
import { ResponseUtil } from '../../utils';
import { ResponseCode } from '../../enums';

export default (app: express.Application, log: Logger): void => {
  // NOTE: The 'next' variable is unused, but this function won't be called if we don't have it.
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  app.use((error, req, res, next) => {
    log.warn(`Body parser failed to parse json body. ${error}`);

    const errorResponse: ErrorResponse = {
      message: `Invalid json body. ${error.message}`,
      statusCode: ResponseCode.BAD_REQUEST
    };

    const response: ApiResponse = ResponseUtil.createResponseBody({
      data: errorResponse,
      success: false
    });

    res.status(ResponseCode.BAD_REQUEST).send(response);
  });
};
