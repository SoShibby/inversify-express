import * as express from 'express';
import * as helmet from 'helmet';

export default (app: express.Application): void => {
  app.use(helmet());
};
