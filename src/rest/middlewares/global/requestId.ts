import * as express from 'express';
import * as rTracer from 'cls-rtracer';

export default (app: express.Application): void => {
  app.use(rTracer.expressMiddleware());
};
