import * as express from 'express';
import * as cookieSession from 'cookie-session';
import { Config } from '../../../common/interfaces';

export default (app: express.Application, config: Config): void => {
  const sessionKey1 = config.get('auth:session_key_1');
  const sessionKey2 = config.get('auth:session_key_2');

  const sessionConfig = {
    name: 'session.cookie',
    keys: [sessionKey1, sessionKey2],
    maxAge: 24 * 60 * 60 * 1000, // 24 hours
    httpOnly: true,
    sameSite: 'lax',
    domain: config.get('auth:session_domain')
  };

  app.use(cookieSession(sessionConfig));
};
