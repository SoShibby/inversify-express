import * as express from 'express';
import * as bodyParser from 'body-parser';

export default (app: express.Application): void => {
  app.use(bodyParser.json());
};
