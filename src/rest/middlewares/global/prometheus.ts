import * as promBundle from 'express-prom-bundle';
import * as express from 'express';

export default (app: express.Application): void => {
  app.use(
    promBundle({
      includeMethod: true,
      includePath: true,
    })
  );
};
