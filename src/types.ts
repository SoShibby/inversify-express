const TYPES = {
  UserService: Symbol.for('UserService'),
  LoginService: Symbol.for('LoginService'),
  Logger: Symbol.for('Logger'),
  GlobalLogger: Symbol.for('GlobaLogger'),
  Server: Symbol.for('Server'),
  Container: Symbol.for('Container'),
  Config: Symbol.for('Config'),
  UserCollection: Symbol.for('UserCollection'),
  Database: Symbol.for('Database'),
  RequestLoggerMiddleware: Symbol.for('RequestLoggerMiddleware'),
  Sentry: Symbol.for('Sentry'),
  Hash: Symbol.for('Hash'),
  AuthenticatedMiddleware: Symbol.for('AuthenticatedMiddleware'),
  AuthProvider: Symbol.for('AuthProvider'),
  BaseController: Symbol.for('BaseController')
};

export { TYPES };
