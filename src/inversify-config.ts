import { Container } from 'inversify';
import { BaseMiddleware } from 'inversify-express-utils';
import { TYPES } from './types';
import {
  UserService,
  UserCollection,
  Database,
  LoginService,
  Sentry,
  Hash
} from './core/interfaces';
import { Server } from './rest/interfaces';
import { Logger, Config } from './common/interfaces';
import { UserServiceImpl, LoginServiceImpl } from './core/services';
import { LoggerImpl, GlobalLoggerImpl } from './common/loggers';
import ServerImpl from './rest/server';
import ConfigImpl from './config';
import DatabaseImpl from './core/database/db';
import UserCollectionImpl from './core/database/collection/user';
import {
  RequestLoggerMiddleware,
  AuthenticatedMiddleware
} from './rest/middlewares';
import SentryImpl from './common/loggers/sentry';
import HashImpl from './core/hash';

const container = new Container();

container
  .bind<UserService>(TYPES.UserService)
  .to(UserServiceImpl)
  .inRequestScope();

container
  .bind<LoginService>(TYPES.LoginService)
  .to(LoginServiceImpl)
  .inRequestScope();

container.bind<Logger>(TYPES.Logger).to(LoggerImpl).inRequestScope();

container
  .bind<Logger>(TYPES.GlobalLogger)
  .to(GlobalLoggerImpl)
  .inSingletonScope();

container.bind<Server>(TYPES.Server).to(ServerImpl).inSingletonScope();

container.bind<Config>(TYPES.Config).to(ConfigImpl).inSingletonScope();

container.bind<Database>(TYPES.Database).to(DatabaseImpl).inRequestScope();

container
  .bind<BaseMiddleware>(TYPES.RequestLoggerMiddleware)
  .to(RequestLoggerMiddleware)
  .inRequestScope();

container
  .bind<BaseMiddleware>(TYPES.AuthenticatedMiddleware)
  .to(AuthenticatedMiddleware)
  .inRequestScope();

container
  .bind<UserCollection>(TYPES.UserCollection)
  .to(UserCollectionImpl)
  .inRequestScope();

container.bind<Sentry>(TYPES.Sentry).to(SentryImpl).inSingletonScope();

container.bind<Hash>(TYPES.Hash).to(HashImpl).inSingletonScope();

export default container;
