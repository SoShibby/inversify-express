import UserModel from '../database/models/user';
import { User } from '../interfaces';

export default class UserValidator {
  public async validate(user: Partial<User>): Promise<void> {
    await new UserModel(user).validate();
  }
}
