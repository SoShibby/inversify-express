export default interface Hash {
  userPassword(plainTextPassword: string): string;
}
