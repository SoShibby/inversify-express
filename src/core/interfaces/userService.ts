import User from './user';
import PaginatedUsers from './paginatedUsers';

export default interface UserService {
  addUser(user: Partial<User>): Promise<User>;
  findUserById(id: string): User;
  getAllUsers(page: number, pageSize: number): Promise<PaginatedUsers>;
}
