import Pagination from './pagination';

export default interface PaginatedResult {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  items: [any];
  pagination: Pagination;
}
