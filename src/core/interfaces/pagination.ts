export default interface Pagination {
  total: number;
  page: number;
  pageSize: number;
}
