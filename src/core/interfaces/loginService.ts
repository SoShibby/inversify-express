import User from './user';
import Credentials from './credentials';

export default interface LoginService {
  login(credentials: Credentials): Promise<User>;
}
