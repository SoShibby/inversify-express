import User from './user';
import Pagination from './pagination';
import PaginatedResult from './paginatedResult';

export default interface PaginatedUsers extends PaginatedResult {
  items: [User];
  pagination: Pagination;
}
