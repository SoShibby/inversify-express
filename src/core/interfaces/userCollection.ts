import User from './user';
import PartialUser from './partialUser';
import PaginatedUsers from './paginatedUsers';

export default interface UserCollection {
  addUser(user: Partial<User>): Promise<User>;
  findUserById(id: string): User;
  findSingleUser(user: PartialUser): User;
  getAllUsersPaginated(page: number, pageSize: number): Promise<PaginatedUsers>;
}
