import { inject, injectable } from 'inversify';
import { TYPES } from '../../types';
import UserValidator from '../validation/user';
import {
  UserService,
  PaginatedUsers,
  User,
  UserCollection,
  Hash
} from '../interfaces';
import { UserError } from '../errors';

@injectable()
export default class UserServiceImpl implements UserService {
  private userValidator: UserValidator;

  constructor(
    @inject(TYPES.UserCollection) private userCollection: UserCollection,
    @inject(TYPES.Hash) private hash: Hash
  ) {
    this.userValidator = new UserValidator();
  }

  public async addUser(user: Partial<User>): Promise<User> {
    try {
      await this.userValidator.validate(user);
    } catch (error) {
      throw new UserError(error.message, error);
    }

    const userWithHashedPassword = {
      ...user,
      password: this.hash.userPassword(user.password)
    };

    const existingUser = await this.userCollection.findSingleUser({
      username: user.username
    });

    if (existingUser) {
      throw new UserError(
        `A user already exist with the username '${user.username}'.`
      );
    }

    return this.userCollection.addUser(userWithHashedPassword);
  }

  public findUserById(id: string): User {
    return this.userCollection.findUserById(id);
  }

  public getAllUsers(page = 0, pageSize = 10): Promise<PaginatedUsers> {
    if (page < 0) {
      throw new UserError('Page value must be greater or equal to 0.');
    }

    if (pageSize < 1) {
      throw new UserError('Page size must be greater or equal to 1.');
    }

    return this.userCollection.getAllUsersPaginated(page, pageSize);
  }
}
