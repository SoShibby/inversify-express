import { inject, injectable } from 'inversify';
import { TYPES } from '../../types';
import {
  LoginService,
  User,
  UserCollection,
  Credentials,
  Hash
} from '../interfaces';
import { Logger } from '../../common/interfaces';
import { UserError } from '../errors';

@injectable()
export default class LoginServiceImpl implements LoginService {
  constructor(
    @inject(TYPES.UserCollection) private userCollection: UserCollection,
    @inject(TYPES.Hash) private hash: Hash,
    @inject(TYPES.Logger) private log: Logger
  ) {}

  public async login(credentials: Credentials): Promise<User> {
    const { username, password } = credentials;

    if (!username || !password) {
      throw new UserError('You must provide a username and password to login.');
    }

    const user = await this.userCollection.findSingleUser({
      username,
      password: this.hash.userPassword(password)
    });

    if (!user) {
      throw new UserError('Invalid username and/or password.');
    }

    this.log.info('User provided valid login credentials.', {
      user: {
        id: user._id
      }
    });

    return user;
  }
}
