import { inject, injectable } from 'inversify';
import UserModel from '../models/user';
import { User, UserCollection, PaginatedUsers } from '../../interfaces';
import { Logger } from '../../../common/interfaces';
import { TYPES } from '../../../types';

@injectable()
export default class UserCollectionImpl implements UserCollection {
  @inject(TYPES.Logger) private log: Logger;

  public async addUser(user: Partial<User>): Promise<User> {
    this.log.info('Adding user to database.');
    const dbUser: User = await new UserModel(user).save();
    this.log.info('Successfully added user.');
    return dbUser;
  }

  public findUserById(id: string): User {
    return UserModel.findById(id).exec();
  }

  public findSingleUser(user: Partial<User>): User {
    return UserModel.findOne(user).exec();
  }

  public async getAllUsersPaginated(
    page: number,
    pageSize: number
  ): Promise<PaginatedUsers> {
    const users = await UserModel.find()
      .skip(page * pageSize)
      .limit(pageSize)
      .exec();

    const totalCount = await UserModel.countDocuments();

    return {
      items: users,
      pagination: {
        total: totalCount,
        page,
        pageSize
      }
    };
  }
}
