import * as mongoose from 'mongoose';
import { inject, injectable } from 'inversify';
import { TYPES } from '../../types';
import { Database } from '../interfaces';
import { Logger, Config } from '../../common/interfaces';
import { InternalError } from '../errors';

@injectable()
export default class DatabaseImpl implements Database {
  @inject(TYPES.GlobalLogger) private log: Logger;

  @inject(TYPES.Config) private config: Config;

  public async connect(): Promise<void> {
    const host = this.config.get('mongodb:host');
    const db = this.config.get('mongodb:db');

    this.log.info(
      `Connecting to mongodb at "${host}" with database name "${db}".`
    );

    // Connect to mongodb.
    await mongoose.connect(`${host}/${db}`, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

    // Log any error that occurs after the initial connection https://mongoosejs.com/docs/connections.html.
    mongoose.connection.on('error', err => {
      this.log.error(
        new InternalError(
          'An unexpected error occurred in Mongodb connection.',
          err
        )
      );
    });

    this.log.info('Successfully connected to mongodb.');
  }

  public async disconnect(): Promise<void> {
    mongoose.connection.close();
  }
}
