import RethrowableError from './rethrowableError';

export default class UnauthorizedError extends RethrowableError {
  constructor(message, error?) {
    super(message, error);

    // We must set the prototype otherwise we can't use instanceof. I.e.
    // const temp = new UnauthorizedError(); console.log(temp instanceof UnauthorizedError);
    // Would return false.
    Object.setPrototypeOf(this, UnauthorizedError.prototype);
  }
}
