import ExtendableError from './extendableError';

class RethrowableError extends ExtendableError {
  constructor(message, error) {
    super(message);

    if (error) {
      const messageLines = (this.message.match(/\n/g) || []).length + 1;
      this.stack = `${this.stack
        .split('\n')
        .slice(0, messageLines + 1)
        .join('\n')}\n${error.stack}`;
    }
  }
}

export default RethrowableError;
