import RethrowableError from './rethrowableError';

export default class UserError extends RethrowableError {
  constructor(message, error?) {
    super(message, error);

    // We must set the prototype otherwise we can't use instanceof. I.e.
    // const temp = new UserError(); console.log(temp instanceof UserError);
    // Would return false.
    Object.setPrototypeOf(this, UserError.prototype);
  }
}
