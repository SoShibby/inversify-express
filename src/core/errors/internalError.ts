import RethrowableError from './rethrowableError';

export default class InternalError extends RethrowableError {
  constructor(message, error?) {
    super(message, error);

    // We must set the prototype otherwise we can't use instanceof. I.e.
    // const temp = new InternalError(); console.log(temp instanceof InternalError);
    // Would return false.
    Object.setPrototypeOf(this, InternalError.prototype);
  }
}
