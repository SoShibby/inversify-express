import { inject, injectable } from 'inversify';
import { HashUtil } from './utils';
import { Config } from '../common/interfaces';
import { TYPES } from '../types';

@injectable()
class Hash {
  constructor(@inject(TYPES.Config) private config: Config) {}

  public userPassword(plainTextPassword: string): string {
    const salt = this.config.get('password_salt');
    return HashUtil.sha512(plainTextPassword, salt);
  }
}

export default Hash;
