export default {
  // Try to parse a string to a number,
  // if the string isn't a number then undefined is returned.
  parseNumber: (str: string | undefined): number | undefined => {
    if (typeof str !== 'string') {
      return undefined;
    }

    const result = Number(str);

    if (Number.isNaN(result)) {
      return undefined;
    }

    return result;
  }
};
