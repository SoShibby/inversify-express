import * as crypto from 'crypto';
import { InternalError } from '../errors';

class HashUtil {
  public static sha512(str: string, salt: string): string {
    try {
      const hash = crypto.createHmac('sha512', salt);
      hash.update(str);
      return hash.digest('hex');
    } catch (err) {
      throw new InternalError(
        `Failed to hash (SHA-512) value. ${err.message}`,
        err
      );
    }
  }
}

export default HashUtil;
