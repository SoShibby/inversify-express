export default {
  isString: (str): boolean => {
    return typeof str === 'string';
  },
  asString: (obj): string => {
    return JSON.stringify(obj);
  },
  truncateString: (str: string, maxLength: number): string => {
    if (str.length > maxLength) {
      return `${str.substr(0, maxLength)}...`;
    }

    return str;
  }
};
