import * as Prometheus from 'prom-client';
import 'reflect-metadata';
import container from './inversify-config';
import { TYPES } from './types';
import { Logger, Config } from './common/interfaces';
import { Server } from './rest/interfaces';
import { Database } from './core/interfaces';
import { InternalError } from './core/errors';

const log = container.get<Logger>(TYPES.GlobalLogger);
const config = container.get<Config>(TYPES.Config);
const server = container.get<Server>(TYPES.Server);
const database = container.get<Database>(TYPES.Database);

const start = async (): Promise<void> => {
  Prometheus.collectDefaultMetrics();

  log.info(`Application configuration: ${config.prettyPrint()}`);

  try {
    await database.connect();
  } catch (e) {
    log.error(new InternalError('Failed to connect to database.', e));
    process.exit(1);
  }

  server.start(container);
};

start();
