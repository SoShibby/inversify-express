import * as winston from 'winston';
import * as rTracer from 'cls-rtracer';
import * as chalk from 'chalk';
import { Config } from '../interfaces';
import { LogType } from '../enums';

const createWinstonLogger = (config: Config): winston.Logger => {
  const { json, printf, combine, timestamp } = winston.format;

  // Create a logger that outputs to the console.
  const logger = winston.createLogger({
    level: config.get('log:level'),
    transports: [new winston.transports.Console()]
  });

  const addDefaultLogType = winston.format(info => {
    if (!info.type) {
      return {
        type: LogType.LOG,
        ...info
      };
    }

    return info;
  });

  // Add request id to the log object.
  const addRequestId = winston.format(info => {
    const requestId = rTracer.id();

    if (requestId) {
      return {
        ...info,
        request: {
          ...info.request,
          id: requestId
        }
      };
    }

    return info;
  });

  const colorize = (str, level): string => {
    if (level === 'error') {
      return chalk.red(str);
    }

    if (level === 'warn') {
      return chalk.yellow(str);
    }

    return str;
  };

  const stringFormat = printf(log => {
    const { timestamp: logTimestamp, level, request, user } = log;

    const metadata = `${logTimestamp} [${level}] [req_id=${
      (request && request.id) || ''
    }] [user_id=${(user && user.id) || ''}]: `;

    let { message } = log;

    if (log.stack) {
      message += ` Stack: ${log.stack}`;
    }

    return colorize(metadata + message, log.level);
  });

  if (process.env.NODE_ENV === 'production') {
    // Output the log object as json when in production.
    // It will be easier to parse if we are using a centralized logging system.
    logger.format = combine(addRequestId(), addDefaultLogType(), json());
  } else {
    // Output the log object as a string when in dev mode.
    // This is so the output will be easier to read for the developer.
    logger.format = combine(
      timestamp(),
      addDefaultLogType(),
      addRequestId(),
      stringFormat
    );
  }

  return logger;
};

export { createWinstonLogger };
