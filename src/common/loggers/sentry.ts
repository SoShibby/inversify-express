import * as Sentry from '@sentry/node';
import { inject, injectable } from 'inversify';
import { TYPES } from '../../types';
import { Config, LogContext } from '../interfaces';

@injectable()
class SentryImpl {
  constructor(@inject(TYPES.Config) private config: Config) {
    if (this.isEnabled()) {
      const dsn = this.config.get('sentry:dsn');
      const environment = this.config.get('environment');

      Sentry.init({
        dsn,
        environment
      });
    }
  }

  public async report(
    messageOrError: string | Error,
    context: LogContext | undefined
  ): Promise<string | undefined> {
    if (!this.isEnabled()) {
      return undefined;
    }

    let eventId;

    Sentry.withScope(scope => {
      if (context) {
        scope.setUser(context.user);
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        scope.setExtras(context as Record<string, any>);
      }

      if (typeof messageOrError === 'string') {
        eventId = Sentry.captureMessage(messageOrError);
      } else {
        eventId = Sentry.captureException(messageOrError);
      }
    });

    await Sentry.flush();

    return eventId;
  }

  private isEnabled(): boolean {
    return this.config.get('sentry:enabled');
  }
}

export default SentryImpl;
