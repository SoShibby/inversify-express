import { injectable, inject } from 'inversify';
import { interfaces, injectHttpContext } from 'inversify-express-utils';
import * as winston from 'winston';
import { TYPES } from '../../types';
import { Config, Logger, LogContext, Sentry } from '../interfaces';
import { createWinstonLogger } from './winston';
import { HttpContextUtil } from '../../rest/utils';

@injectable()
export default class LoggerImpl implements Logger {
  private internalLogger: winston.Logger;

  constructor(
    @inject(TYPES.Config) config: Config,
    @injectHttpContext private readonly httpContext: interfaces.HttpContext,
    @inject(TYPES.Sentry) private sentry: Sentry
  ) {
    this.internalLogger = createWinstonLogger(config);
  }

  public debug(messageOrError: string | Error, context?: LogContext): void {
    this.log('debug', messageOrError, context);
  }

  public info(messageOrError: string | Error, context?: LogContext): void {
    this.log('info', messageOrError, context);
  }

  public warn(messageOrError: string | Error, context?: LogContext): void {
    this.log('warn', messageOrError, context);
  }

  public error(messageOrError: string | Error, context?: LogContext): void {
    this.log('error', messageOrError, context);
  }

  public log(
    level: string,
    messageOrError: string | Error,
    context?: LogContext
  ): void {
    const logContext = this.createLogContext(messageOrError, context);
    const logMessage = this.getLogMessage(messageOrError);

    this.internalLogger.log({
      level,
      message: logMessage,
      ...logContext
    });

    if (level === 'error') {
      this.sentry.report(messageOrError, context);
    }
  }

  private getLogMessage(messageOrError: string | Error): string {
    if (messageOrError instanceof Error) {
      return messageOrError.message;
    }

    return messageOrError;
  }

  private createLogContext(
    messageOrError: string | Error,
    context: LogContext
  ): LogContext {
    const userId = HttpContextUtil.getUserId(this.httpContext);

    return {
      ...context,
      ...(messageOrError instanceof Error && { stack: messageOrError.stack }),
      ...(userId && { user: { id: userId } })
    };
  }
}
