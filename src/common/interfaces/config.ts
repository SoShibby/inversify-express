export default interface Config {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get(key: string): any;
  prettyPrint(): string;
}
