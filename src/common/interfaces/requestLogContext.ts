export default interface RequestLogContext {
  id?: string;
  method?: string;
  url?: string;
  body?: string;
}
