export default interface Sentry {
  report(
    messageOrError: string | Error,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    context: Record<string, any>
  ): Promise<string | undefined>;
}
