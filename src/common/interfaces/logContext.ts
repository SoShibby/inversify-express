import { LogType } from '../enums';
import UserLogContext from './userLogContext';
import RequestLogContext from './requestLogContext';
import ResponseLogContext from './responseLogContext';

export default interface LogContext {
  type?: LogType;
  user?: UserLogContext;
  request?: RequestLogContext;
  response?: ResponseLogContext;
}
