import { LogContext } from './logContext';

export default interface Logger {
  debug(messageOrError: string | Error, context?: LogContext);
  info(messageOrError: string | Error, context?: LogContext);
  warn(messageOrError: string | Error, context?: LogContext);
  error(messageOrError: string | Error, context?: LogContext);
  log(
    level: string,
    messageOrError: string | Error,
    context?: LogContext
  ): void;
}
