export default interface ResponseLogContext {
  statusCode?: number;
  responseTime?: number;
  contentLength?: number;
}
