enum LogType {
  LOG = 'log',
  ERROR_BOUNDARY = 'errorBoundary',
  REQUEST = 'request',
  RESPONSE = 'response'
}

export default LogType;
