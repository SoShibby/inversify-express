import * as nconf from 'nconf';
import * as nconfYaml from 'nconf-yaml';
import * as prettyjson from 'prettyjson';
import { injectable } from 'inversify';
import { Config } from './common/interfaces';

@injectable()
export default class ConfigImpl implements Config {
  private SENSITIVE_INFORMATION = '[redacted because of sensitive information]';

  constructor() {
    this.configureNConf();
  }

  private configureNConf(): void {
    if (process.env.NODE_ENV === 'development') {
      nconf.argv().env().file({
        file: 'config.dev.yaml',
        format: nconfYaml
      });
    } else {
      nconf.argv().env();
    }

    nconf.env({
      parseValues: true,
      separator: '__',
      whitelist: [
        'port',
        'log__level',
        'cors__origin',
        'mongodb__host',
        'mongodb__db',
        'auth__secret',
        'auth__session_key_1',
        'auth__session_key_2',
        'auth__session_domain',
        'sentry__enabled',
        'sentry__dsn',
        'environment',
        'password_salt',
        'NODE_ENV'
      ]
    });

    nconf.defaults({
      port: 3000,
      log: {
        level: 'info'
      },
      cors: {
        origin: '*'
      },
      sentry: {
        enabled: false
      }
    });

    nconf.required([
      'port',
      'log:level',
      'cors:origin',
      'mongodb:host',
      'mongodb:db',
      'auth:secret',
      'auth:session_key_1',
      'auth:session_key_2',
      'auth:session_domain',
      'environment',
      'password_salt',
      'NODE_ENV'
    ]);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public get(key: string): any {
    return nconf.get(key);
  }

  public prettyPrint(): string {
    const allConfigurations = nconf.get();

    // When doing nconf.get() we get some additional properties which aren't configuration
    // properties. So we need to remove those extra properties before we output our json object.
    delete allConfigurations.type;
    delete allConfigurations._;
    delete allConfigurations.$0;

    if (process.env.NODE_ENV === 'production') {
      allConfigurations.auth.session_key_1 = this.SENSITIVE_INFORMATION;
      allConfigurations.auth.session_key_2 = this.SENSITIVE_INFORMATION;
      allConfigurations.auth.secret = this.SENSITIVE_INFORMATION;
      allConfigurations.password_salt = this.SENSITIVE_INFORMATION;
      allConfigurations.mongodb.password = this.SENSITIVE_INFORMATION;

      return JSON.stringify(allConfigurations, null, 2);
    }

    return prettyjson.render(allConfigurations);
  }
}
