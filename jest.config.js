const tsPreset = require('ts-jest/jest-preset');
const jestMongodbPreset = require('@shelf/jest-mongodb/jest-preset');

module.exports = {
  ...tsPreset,
  ...jestMongodbPreset,
  testEnvironment: 'node',
  testMatch: ['**/test/**/(*.)test.ts'],
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.json'
    }
  }
};
