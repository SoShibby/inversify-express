FROM node:13.1.0-alpine AS dependencies

WORKDIR /app

COPY package* ./

# Install dependencies that we will use in the final Docker image.
RUN npm ci --only=production && cp -R node_modules /node_modules_production

# Install dev dependencies. These are dependencies that is needed in the test and build phase of the Dockerfile.
RUN npm ci

FROM dependencies as test

COPY src src
COPY .eslintrc ./

RUN npm run lint

FROM dependencies as build

WORKDIR /app

COPY src src
COPY tsconfig.json .

RUN npm run build

FROM node:13.1.0-alpine as release

WORKDIR /app

COPY --from=dependencies /node_modules_production node_modules
COPY --from=build /app/build build
COPY healthcheck.js .

HEALTHCHECK --interval=10s --timeout=2s --start-period=15s \  
  CMD node healthcheck.js

CMD [ "node", "build/index.js" ]

