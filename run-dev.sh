#!/bin/bash

# The commit sha will be appended to all log output and can be viewed in Kibana.
export CI_COMMIT_SHA=`git rev-parse --short HEAD`

sudo CI_COMMIT_SHA=$CI_COMMIT_SHA docker-compose -f docker-compose-dev.yaml build
sudo CI_COMMIT_SHA=$CI_COMMIT_SHA docker-compose -f docker-compose-dev.yaml up
